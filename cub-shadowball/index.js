const { MessageEmbed, Intents, Client } = require('discord.js');
const fs = require('fs');
const client = new Client({intents:[
    Intents.FLAGS.GUILDS,
    Intents.FLAGS.GUILD_MESSAGES
]});

const { token, prefix } = require('./system/config.json');
const calculateDifference = (guess, value) => {
__dirname = path.resolve();
/*    val1 = guess - value;
    val2 = (1000 - value) + guess;
    val3 = value - guess;

    if(val1 < 0) {
        val1 = 999999999;
    } else if(val2 < 0) {
        val2 = 9999999;
    } else if(val3 < 0) {
        val3 = 9999999;
    }
   
    if(val1 < val2 && val1 < val3) {
        return val1;
    } else if(val2 < val1 && val2 < val3) {
        return val2;
    } else {
        return val3;
    }*/

    diff = Math.abs(guess - value);
    if(diff>500) {
        return (1000 - diff);
    }
    else {
        return diff;
    }
};

client.on("ready", () => {
    console.log(`--> Bot online`);
});
client.on("messageCreate", async message => {

    if(message.author.bot) return;

    msg = message.content.toLowerCase();
    md = message.content.split(" ");

    if(msg.startsWith(`${prefix}s`)) {
        
        if(isNaN(md[1]) || md[1] < 1 || md[1] > 1000) {
            embed = new MessageEmbed()
                .setColor('DARK_RED')
                .setAuthor(message.author.tag, message.author.displayAvatarURL())
                .setDescription(`Please enter a number, greater than 1, but less than 1000.`)
            message.reply({embeds:[embed]});
            return
        }
        data = JSON.parse(fs.readFileSync(`./system/guesses.json`));
        if(data.filter(t => t.id == message.author.id)[0]) {
            embed = new MessageEmbed()
                .setColor('DARK_RED')
                .setAuthor(message.author.tag, message.author.displayAvatarURL())
                .setDescription(`You have already guessed.`)
            message.reply({embeds:[embed]});
        }

        embed = new MessageEmbed()
            .setColor('DARK_BLUE')
            .setAuthor(message.author.tag, message.author.displayAvatarURL())
            .setDescription(`Your guess has been entered. Good luck!`)
        message.reply({embeds:[embed]});

        data.push({
            id: message.author.id,
            guess: parseInt(md[1]),
            tag: message.author.tag,
        });
        fs.writeFileSync(`./system/guesses.json`, JSON.stringify(data, null, 4));

        if(fs.existsSync(`./system/data/${message.author.id}.json`)) {
            data = JSON.parse(fs.readFileSync(`./system/data/${message.author.id}.json`));
            data.tag = message.author.tag;
            fs.writeFileSync(`./system/data/${message.author.id}.json`, JSON.stringify(data, null, 4));
        } else {
            fs.writeFileSync(`./system/data/${message.author.id}.json`, JSON.stringify({
                points: 0,
                tag: message.author.tag
            }, null, 4));
        }
    }
    if(msg.startsWith(`${prefix}p`) && message.member.permissions.has('ADMINISTRATOR')) {
        
        if(isNaN(md[1]) || md[1] < 1 || md[1] > 1000) {
            embed = new MessageEmbed()
                .setColor('DARK_RED')
                .setAuthor(message.author.tag, message.author.displayAvatarURL())
                .setDescription(`Please enter a number, greater than 1, but less than 1000.`)
            message.reply({embeds:[embed]});
            return
        }

        embed = new MessageEmbed()
            .setColor('DARK_BLUE')
            .setAuthor(message.author.tag, message.author.displayAvatarURL())
            .setDescription(`Set! Guesses have been rewarded points as appropriate.`)
        message.reply({embeds:[embed]});

        data1 = JSON.parse(fs.readFileSync(`./system/guesses.json`));
        for(i = 0; i < data1.length; i++) {

            current = data1[i];
            difference = calculateDifference(current.guess, parseInt(md[1]));

            points = 0;
            if(difference <= 25) {
                points = 4;
            } else if(difference <= 50) {
                points = 3;
            } else if(difference <= 100) {
                points = 2;
            } else if(difference <= 200) {
                points = 1;
            }

            if(fs.existsSync(`./system/data/${current.id}.json`)) {
                data = JSON.parse(fs.readFileSync(`./system/data/${current.id}.json`));
                data.points += points;
                fs.writeFileSync(`./system/data/${current.id}.json`, JSON.stringify(data));
            } else {
                fs.writeFileSync(`./system/data/${current.id}.json`, JSON.stringify({
                    points: points,
                    tag: current.tag
                }));
            }
        }

        fs.writeFileSync(`./system/guesses.json`, JSON.stringify([], null, 4));
    }
    if(msg == `${prefix}leaderboard`) {
        files = fs.readdirSync(`./system/data/`).filter(t => t.endsWith('.json'));
        unsorted = [];
        for(let i in files) {
            file = JSON.parse(fs.readFileSync(`./system/data/${files[i]}`));
            unsorted.push({
                id: files[i].split(".")[0],
                count: file.points,
                tag: file.tag
            });
        };
        leaderboard = unsorted.sort((a, b) => (a.count < b.count) ? 1 : -1);    
        
        // Further sort
        raw = [];
        counter = 1
        for(let i in leaderboard) {
            raw.push({
                id: leaderboard[i].id,
                username: leaderboard[i].tag,
                position: counter,
                count: leaderboard[i].count,
            });
            counter = counter + 1;
        }

        // Format
        pages = [];
        string = [];
        for(let i in raw) {
            temp = `${raw[i].position}) ${raw[i].username} - ${raw[i].count} points`;
            if(raw[i].position == 1) {
                temp = `🥇 ${raw[i].username} - ${raw[i].count} points`
            } else if(raw[i].position == 2) {
                temp = `🥈 ${raw[i].username} - ${raw[i].count} points`
            } else if(raw[i].position == 3) {
                temp = `🥉 ${raw[i].username} - ${raw[i].count} points`
            }
            if(string.length < 10) {
                string.push(temp);
            } else {
                pages.push("```"+ string.join(` \n`) +"\n```");
                string = [];
                string.push(temp);
            }
        }

        pages = pages.slice(10);

        // Any users?
        if(string.length == 0) {
            embed = new MessageEmbed()
                .setAuthor(`Leaderboard`, message.guild.iconURL())
                .setColor('DARK_BLUE') 
                .setDescription("`No results to show.`")
            message.channel.send({embeds:[embed]});
            return
        }

        // Less than 10 users?
        if(pages.length == 0) {
            pages.push("```"+ string.join(` \n`) +"\n```");
        }

        embed = new MessageEmbed()
            .setAuthor(`Leaderboard`, message.guild.iconURL())
            .setColor('DARK_BLUE') 
            .setDescription(pages[0])
        message.channel.send({embeds:[embed]});
    }
    if(msg == `${prefix}reset` && message.member.permissions.has('ADMINISTRATOR')) {
        embed = new MessageEmbed()
            .setColor('DARK_RED')
            .setAuthor(message.author.tag, message.author.displayAvatarURL())
            .setDescription(`System has been reset!`)
        message.reply({embeds:[embed]});

        fs.writeFileSync(`./system/guesses.json`, JSON.stringify([], null, 4));

        files = fs.readdirSync(`./system/data/`).filter(t => t.endsWith(`.json`));
        files.forEach(t => {
            fs.unlinkSync(`./system/data/${t}`);
        });
    }
});

client.login(token);
